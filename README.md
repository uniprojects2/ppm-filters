# PPM Filters

## Description
A project made for the course "Computer Programming in C++"

This is a command-line program that applies several filters to a PPM image 

simply choose a .ppm image and apply any combination of filters by executing the command 

"ex -f filter_name1 val1 /image.ppm", where filter_name is either "linear", "gamma" or "blur" and val is a filter-specific parameter 

The filters can be applied one after another on the same picture 
